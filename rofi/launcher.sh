#!/usr/bin/env bash

## Author  : Aditya Shakya
## Mail    : adi1090x@gmail.com
## Github  : @adi1090x
## Twitter : @adi1090x
## 
## 

## Uncomment ONE AND ONLY ONE of the commands below ##
## Comment the other one to avoid execution issues ##

## This command will omit config.rasi and will use launcher.rasi instead 
# rofi -no-config -no-lazy-grab -show drun -modi drun -theme ~/.config/rofi/launcher/launcher.rasi

## This command will use config.rasi directly
rofi -no-lazy-grab -show drun -modi drun

## This command will omit config.rasi and will use powermenu.rasi instead 
# rofi -no-config -no-lazy-grab -show drun -modi drun -theme ~/.config/rofi/launcher/powermenu.rasi
