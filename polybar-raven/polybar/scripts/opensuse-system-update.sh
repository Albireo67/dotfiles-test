#!/bin/sh

# Lanzamos comando de actualización completa del sistema
echo "Iniciando actualización del sistema"
echo "Introducir clave de usuario:"

sudo zypper refresh && sudo zypper dup

# Verificamos dependencias del sistema

sudo zypper ve && exit

exit
